import time
from threading import Thread

import cv2
import numpy as np

font = cv2.FONT_HERSHEY_COMPLEX


class VideoStream:

    def __init__(self):

        # initialize the file video stream along with the boolean
        # used to indicate if the thread should be stopped or not

        self.stream = cv2.VideoCapture(1)
        self.stream.set(cv2.CAP_PROP_BUFFERSIZE, 2)
        (self.grabbed, self.frame) = self.stream.read()
        self.stopped = False

        # initialize the queue used to store frames read from
        # the video file

    def start(self):
        Thread(target=self.update, args=()).start()
        return self

    def update(self):

        # keep looping infinitely until the thread is stopped

        while True:

            # if the thread indicator variable is set, stop the thread

            if self.stopped:
                return

            # otherwise, read the next frame from the stream

            (self.grabbed, self.frame) = self.stream.read()

    def read(self):

        # return the frame most recently read

        return self.frame

    def stop(self):

        # indicate that the thread should be stopped

        self.stopped = True


confThreshold = 0.5
nmsThreshold = 0.2

#cap = cv2.VideoCapture(0)

# Load Yolo

modelConfiguration = './cfg/yolov3.cfg'
modelWeights = './weights/yolov3.weights'

faceMaskConfiguration = './cfg/yolov3-face_mask.cfg'
faceMaskModelWeights = './weights/yolov3-face_mask.weights'

net = cv2.dnn.readNetFromDarknet(modelConfiguration, modelWeights)

facenet = cv2.dnn.readNetFromDarknet(faceMaskConfiguration,
                                     faceMaskModelWeights)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

classes = []
classes_masks = []

with open('./classes/coco.names', 'r') as f:
    classes = [line.strip() for line in f.readlines()]

with open('./classes/mask_classes.names', 'r') as f:
    classes_masks = [line.strip() for line in f.readlines()]

layer_names = net.getLayerNames()
layer_names_face = facenet.getLayerNames()
output_layers = [layer_names[i - 1] for i in
                 net.getUnconnectedOutLayers()]
output_layers_face = [layer_names_face[i - 1] for i in
                      facenet.getUnconnectedOutLayers()]
colors = np.random.uniform(0, 255, size=(len(classes), 3))


def detectObject(outputs, img):
    class_ids = []
    confidences = []
    boxes = []
    for out in outputs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > confThreshold:
                # Object detected

                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Rectangle coordinates

                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    indexes = cv2.dnn.NMSBoxes(boxes, confidences, confThreshold,
                               nmsThreshold)

    person_count = 0
    for i in range(len(boxes)):
        if i in indexes:
            if str(classes[class_ids[i]]) == 'person':
                person_count = person_count + 1

    cv2.putText(
        img,
        'No of People: ' + str(person_count),
        (10, 20),
        font,
        0.75,
        (0, 0, 0),
        2,
    )

    for i in range(len(boxes)):
        if i in indexes:
            (x, y, w, h) = boxes[i]
            label = str(classes[class_ids[i]])
            text = label + ' ' + str(int(confidences[i] * 100)) + '%'
            color = colors[class_ids[i]]
            cv2.rectangle(img, (x, y), (x + w, y + h), color, 3)
            cv2.putText(
                img,
                text,
                (x, y - 3),
                font,
                0.75,
                color,
                2,
            )


def detectFaceMask(outputs, img):
    class_ids = []
    confidences = []
    boxes = []
    for out in outputs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > confThreshold:
                # Object detected

                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Rectangle coordinates

                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    indexes = cv2.dnn.NMSBoxes(boxes, confidences, confThreshold,
                               nmsThreshold)

    peopleWithMasks = 0
    peopleWithNoMasks = 0

    for i in range(len(boxes)):
        if i in indexes:
            if str(classes_masks[class_ids[i]]) == 'Mask':
                peopleWithMasks = peopleWithMasks + 1
            elif str(classes_masks[class_ids[i]]) == 'No Mask':
                peopleWithNoMasks = peopleWithNoMasks + 1

    cv2.putText(
        img,
        'People with masks: ' + str(peopleWithMasks)
        + ', People with no masks: ' + str(peopleWithNoMasks),
        (10, 40),
        font,
        0.75,
        (0, 0, 0),
        2,
    )

    for i in range(len(boxes)):
        if i in indexes:
            (x, y, w, h) = boxes[i]
            label = str(classes_masks[class_ids[i]])
            text = label + ' ' + str(int(confidences[i] * 100)) + '%'
            color = colors[class_ids[i]]
            cv2.rectangle(img, (x, y), (x + w, y + h), color, 3)
            cv2.putText(
                img,
                text,
                (x, y - 3),
                font,
                0.75,
                color,
                2,)


vs = VideoStream().start()

while True:

    # Loading image
    # success, img = cap.read()

    startTime = time.time()
    print("start Time: ", startTime)
    frame = vs.read()
    (height, width, channels) = frame.shape
    img = frame[0:height,0:(int)(width/2)]
    height, width, channels = img.shape

    # Detecting objects

    blob = cv2.dnn.blobFromImage(
        img,
        0.00392,
        (320, 320),
        (0, 0, 0),
        True,
        crop=False,
    )

    net.setInput(blob)
    facenet.setInput(blob)
    outs = net.forward(output_layers)
    outsface = facenet.forward(output_layers_face)

    detectObject(outs, img)
    print("Object Detect end Time: ", time.time() - startTime)
    detectFaceMask(outsface, img)
    print("Face Detect end Time: ", time.time() - startTime)

    cv2.imshow('Image', img)
    key = cv2.waitKey(1)
    if key == 27:  # esc key stops the process
        break
    print("End Time: ", time.time() - startTime)

cap.release()
cv2.destroyAllWindows()
fvs.stop()
