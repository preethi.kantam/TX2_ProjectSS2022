# AI Object Detection and Distance Measurement on Nvidia Jetson TX2

This project implements object and face mask detection with distance estimation using the Nvidia Jetson TX2 board with the ZED-Camera.

## Introduction
Artificial Intelligence (AI) operations are gaining in interest in the last couple of years. Since hardware is rapidly becoming cheaper and more accessible and also software is getting more advanced, AI operations can nowadays be found everywhere. Starting with 
Virtual Assistant or Chatbots, Security and Surveillance all the way to self-driving cars or autonomous vehicles.

## Models used for implementing the use case
1. YOLOv3
2. YOLOv3-Tiny
3. YOLOv4
4. YOLOv4-Tiny

## Installation

Note: Check the software versions currently on the TX2 board. If all versions match then continue with "Running the Implementation" section.

Step1: Install Jetpack3.3 on TX2 board (This step is required only on new TX2 board)

Note: An Ubuntu host PC is required to run the JetPack installer. It can flash and update software on a target Jetson device, but it cannot not run directly on that device. When no target device is present, the JetPack installer can still be used to update software on the host PC.

Refer documentation [NVIDIA JetPack Documentation - JetPack](https://docs.nvidia.com/jetson/archives/jetpack-archived/jetpack-33/index.html#jetpack/3.3/introduction.htm%3FTocPath%3D_____1) for installation of Jetpack 3.3 on Tx2. (Check all the options while prompted).

Step2:  Install Python 3.7
Refer installation tutorial [Install python on Ubuntu](https://www.youtube.com/watch?v=7H-DcdSmV0U)

Step3: PIP install below libraries        
```bash
sudo pip install numpy==1.21.6           
sudo pip install opencv-python==4.6.0.66
```

Step4: Install ZED sdk version 2.8.5
Download ZED SDK for Jetpack 3.3 2.8.5 (Jetson TX1, TX2, CUDA 9) from [ZED SDK 2.8 - Download | Stereolabs](https://www.stereolabs.com/developers/release/2.8/), follow installation steps provided in [How to Install ZED SDK on Linux | Stereolabs](https://www.stereolabs.com/docs/installation/linux/) . and in the readme file of the downloaded zip file.

## Running the Implementation
1. Download the zip file or clone the project.
2. Go-to the project folder
3. Right click and open terminal
4. Enter below commands

YOLOv3 Model - Live feed from ZED Camera
```bash
python3.7 SS2022_yolov3_zed.py
```
YOLOv3-tiny Model - Live feed from ZED Camera
```bash
python3.7 SS2022_yolov3_tiny_zed.py
```
YOLOv4 Model - Live feed from ZED Camera
```bash
python3.7 SS2022_yolov4_zed.py
```
YOLOv4-tiny Model - Live feed from ZED Camera
```bash
python3.7 SS2022_yolov4_tiny_zed.py
```
## Benchmark Testing
The maximum frame rate that we have is really low, which is what we could already anticipate from watching the video recordings. By including the tiny version to the model, we were able to increase the frames per second by a factor of 7, although the model still 
runs extremely slowly. Whether you use YOLO version 3 or version 4 for performance, there isn't much of a difference in terms of frame rates.

It makes also a big difference which image size the algorithm uses. For smaller images (160*160 pixels) the frame rate is way higher than for bigger images (320*320 pixels), but taking smaller images will also result in less accurate object detection.

We could at least determine that running the performance test on a laptop (Processor: AMD Ryzen 7 5700U with Radeon Graphics            1.80 GHz, RAM: 16GB,OS: Windows 11) would result in frame rates of up to 25. In this area, our project might be enhanced, it could be optimized for the Nvidia Tx2 in the future.

## Conclusion
In this work, we proposed an approach for object detection and distance estimation for real time application by leveraging recent advances in deep neural networks and the availability of high performance pre-trained models. This system can be installed in crowded areas which would help to get the number of people wearing mask on and not. 

The difficulty faced during the object detection is finding the right balance between speed and accuracy. The latency, speed and accuracy for each detection stage is monitored with the combination of different models of YOLO. The deviation between the obtained distance of the object from the camera and the measured distance of the object from the camera is less.

From the results obtained it could be concluded that the accuracy of the system is better but the speed is low. The proposed approach for object detection could be further developed and improved for better performance and speed.

