import time
from threading import Thread

import cv2
import numpy as np
import pyzed.sl as sl

import math


def zed_init():
    # Create a ZED camera object
    zed = sl.Camera()

    # Set configuration parameters
    init = sl.InitParameters()
    init.camera_resolution = sl.RESOLUTION.RESOLUTION_HD720
    init.depth_mode = sl.DEPTH_MODE.DEPTH_MODE_PERFORMANCE
    init.coordinate_units = sl.UNIT.UNIT_METER

    # Open the camera
    err = zed.open(init)
    if err != sl.ERROR_CODE.SUCCESS:
        print(repr(err))
        zed.close()
        exit(1)

    # Set runtime parameters after opening the camera
    runtime = sl.RuntimeParameters()
    runtime.sensing_mode = sl.SENSING_MODE.SENSING_MODE_STANDARD

    image_zed = sl.Mat()
    depth_image_zed = sl.Mat()
    point_cloud = sl.Mat()

    return zed, image_zed, point_cloud, runtime


font = cv2.FONT_HERSHEY_COMPLEX


class VideoStream:

    def __init__(self):

        # initialize the file video stream along with the boolean
        # used to indicate if the thread should be stopped or not
        
        self.zed, self.image_zed, self.point_cloud,self.runtime = zed_init()
        self.zed.grab(self.runtime)
        self.zed.retrieve_image(self.image_zed, sl.VIEW.VIEW_LEFT)
        self.img = self.image_zed.get_data()
        self.zed.retrieve_measure(self.point_cloud, sl.MEASURE.MEASURE_XYZRGBA)
        self.stopped = False

        # initialize the queue used to store frames read from
        # the video file

    def start(self):
        Thread(target=self.update, args=()).start()
        return self

    def update(self):

        # keep looping infinitely until the thread is stopped

        while True:

            # if the thread indicator variable is set, stop the thread

            if self.stopped:
                return

            # otherwise, read the next frame from the stream

            self.zed.grab(self.runtime)
            self.zed.retrieve_image(self.image_zed, sl.VIEW.VIEW_LEFT)
            self.img = self.image_zed.get_data()
            self.zed.retrieve_measure(self.point_cloud, sl.MEASURE.MEASURE_XYZRGBA)

    def read(self):

        # return the frame most recently read

        return self.img, self.point_cloud

    def stop(self):

        # indicate that the thread should be stopped

        self.stopped = True


confThreshold = 0.5
nmsThreshold = 0.2


# Load Yolo

modelConfiguration = './cfg/yolov4-tiny.cfg'
modelWeights = './weights/yolov4-tiny.weights'

faceMaskConfiguration = './cfg/yolov4-tiny-face_masks.cfg'
faceMaskModelWeights = './weights/yolov4-tiny-face_mask.weights'

net = cv2.dnn.readNetFromDarknet(modelConfiguration, modelWeights)

facenet = cv2.dnn.readNetFromDarknet(faceMaskConfiguration,
                                     faceMaskModelWeights)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA_FP16)

model = cv2.dnn_DetectionModel(net)
modelFace = cv2.dnn_DetectionModel(facenet)

LABELS = []
LABELS_MASK = []

with open('./classes/coco.names', 'r') as f:
    LABELS = [line.strip() for line in f.readlines()]

with open('./classes/mask_classes.names', 'r') as f:
    LABELS_MASK = [line.strip() for line in f.readlines()]

layer_names = net.getLayerNames()
layer_names_face = facenet.getLayerNames()
output_layers = [layer_names[i-1] for i in
                 net.getUnconnectedOutLayers()]
output_layers_face = [layer_names_face[i-1] for i in
                      facenet.getUnconnectedOutLayers()]

colors = np.random.uniform(0, 255, size=(len(LABELS),3))


def detectObject_Yolo(modelName,img):
        modelName.setInputParams(size=(416, 416), scale=1/255, swapRB=True)
        image_test = cv2.cvtColor(img, cv2.COLOR_RGBA2RGB)
        image = image_test.copy()
        print('image',image.shape)

        classes, confidences, boxes = modelName.detect(image, confThreshold, nmsThreshold)
        
        return classes,confidences,boxes

vs = VideoStream().start()
framecount = 0
startTime = time.time()

while True:

    # Loading image
    # success, img = cap.read()

    img, pcloud = vs.read()
    cv2.imshow("ZED", img)

    # Detecting objects
    classes,confidences,boxes = detectObject_Yolo(model, img)
    classes_face,confidences_face,boxes_face = detectObject_Yolo(modelFace, img)
    
    font = cv2.FONT_HERSHEY_SIMPLEX
    person_count = 0
    peopleWithMasks = 0
    peopleWithNoMasks = 0

    indexes = cv2.dnn.NMSBoxes(boxes, confidences, confThreshold, nmsThreshold)
    indexes_face = cv2.dnn.NMSBoxes(boxes_face, confidences_face, confThreshold, nmsThreshold)

    for i in range(len(boxes)):
        if i in indexes:
            if str(LABELS[classes[i]]) == "person":
                person_count = person_count + 1
                        
    for i in range(len(boxes_face)):
        if i in indexes_face:
            if str(LABELS_MASK[classes_face[i]]) == "Mask":
                peopleWithMasks = peopleWithMasks + 1
            elif str(LABELS_MASK[classes_face[i]]) == "No Mask":
                peopleWithNoMasks = peopleWithNoMasks + 1

    for cl,score,(left,top,width,height) in zip(classes,confidences,boxes):
        start_pooint = (int(left),int(top))
        end_point = (int(left+width),int(top+height))
                
        x = int(left + width/2)
        y = int(top + height/2)

        color = colors[cl]
        image =cv2.rectangle(img,start_pooint,end_point,color,3)

        cv2.putText(image, "No of People: " + str(person_count), (10, 25), font, 1, (160, 4, 183), 2)
        cv2.putText(image, "People with masks: " + str(peopleWithMasks) + ", People with no masks: " + str(peopleWithNoMasks), (10, 50), font, 1, (160, 4, 183), 2)
        text = f'{LABELS[cl]}'
                
        x = round(x)
        y = round(y)

        err, point_cloud_value = pcloud.get_value(x, y)
        distance = math.sqrt(point_cloud_value[0] * point_cloud_value[0] + point_cloud_value[1] * point_cloud_value[1] + point_cloud_value[2] * point_cloud_value[2])

        if not np.isnan(distance) and not np.isinf(distance):
            cv2.putText(image,text + ':' + str(int(score * 100)) + '%'+" Distance:" + str(round(distance,2))+'m', (int(left), int(top -7)) , font, 0.75, color, 2)
        else:
            cv2.putText(image, text + ':' + str(int(score * 100)) + '%' + " Distance:Invalid", (int(left), int(top-7)), font, 0.75, color, 2 )
            print("Can't estimate distance at this position")

    for cl_face,score_face,(left,top,width,height) in zip(classes_face,confidences_face,boxes_face):
        start_pooint = (int(left),int(top))
        end_point = (int(left+width),int(top+height))
        color = colors[cl_face]
        image =cv2.rectangle(img,start_pooint,end_point,color,3)
        text = f'{LABELS_MASK[cl_face]}'
        cv2.putText(image, text + ':' + str(int(score_face * 100)) + '%', (int(left), int(top-7)), font, 0.75, color, 2)
    

    cv2.imshow("ZED", img)
    framecount= framecount +1
    endtime = time.time() - startTime
    if(endtime >= 1):
       fps = framecount/endtime
       framecount= 0
       print("FPS: ",fps)
       startTime = time.time()
    key = cv2.waitKey(1)
    key = cv2.waitKey(1)
    if key == 27:  # esc key stops the process
        break


cap.release()
cv2.destroyAllWindows()
vs.stop()
